package com.example.hidetoolbar.ui

data class Data(
    val id: Long? = null,
    val name: String? = "Name",
    val description: String? = "Description"
)