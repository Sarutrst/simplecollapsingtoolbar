package com.example.hidetoolbar.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.hidetoolbar.ui.Data
import com.example.hidetoolbar.databinding.ItemListBinding

class MainAdapter(
    private val data: List<Data>
) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MainViewHolder {
        val binding = ItemListBinding.inflate(LayoutInflater.from(p0.context), p0, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        //show only first data
        holder.bind(data[0])
    }

    //dummy data
    override fun getItemCount() = 20

    inner class MainViewHolder(private val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Data) {
            binding.tvName.text = data.name
            binding.tvDescription.text = data.description
        }
    }
}

