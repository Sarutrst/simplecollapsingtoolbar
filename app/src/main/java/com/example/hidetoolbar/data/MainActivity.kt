package com.example.hidetoolbar.data

import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hidetoolbar.adapter.MainAdapter
import com.example.hidetoolbar.R
import com.example.hidetoolbar.ui.Data
import com.example.hidetoolbar.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var mainAdapter: MainAdapter
    private var binding: ActivityMainBinding? = null

    private val travels = mutableListOf(Data())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        mainAdapter = MainAdapter(travels)

        binding?.myRv?.apply {
            adapter = this@MainActivity.mainAdapter
            layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
        }

        binding?.tab1Btn?.setOnClickListener {
            binding?.tab1Ln?.setBackgroundResource(R.drawable.bg_all_roundcormer_onlytop_bottomline_primary)
            binding?.tab2Ln?.setBackgroundResource(R.drawable.bg_all_roundcormer_onlytop_bottomline_grey)

            binding?.tab1Tv?.setTextColor(Color.parseColor("#48D2A0"))
            binding?.tab2Tv?.setTextColor(Color.parseColor("#B5C1D7"))
        }

        binding?.tab2Btn?.setOnClickListener {
            binding?.tab1Ln?.setBackgroundResource(R.drawable.bg_all_roundcormer_onlytop_bottomline_grey)
            binding?.tab2Ln?.setBackgroundResource(R.drawable.bg_all_roundcormer_onlytop_bottomline_primary)

            binding?.tab1Tv?.setTextColor(Color.parseColor("#B5C1D7"))
            binding?.tab2Tv?.setTextColor(Color.parseColor("#48D2A0"))
        }

        binding?.lnCreateStock?.setOnClickListener {
            Toast.makeText(this, "Add Clicked", Toast.LENGTH_SHORT).show()
        }
    }
}